'use strict';
~ function() {
    var $ = TweenMax,
    	power1InOut = Power1.easeInOut,
        ad = document.getElementById('mainContent'),
        ground = document.getElementById('groundContainer'),
        football = document.getElementById('football'),
        footballSpritWidth = 2600,
        tID; 

    window.init = function() {

        var tl = new TimelineMax();
        tl.set(ad, { perspective: 1000, force3D: true })
        tl.set(ground, { y:-250 })
        tl.set("#copy1", { y: -110 })
        tl.set(football,{ x: 480,y:-100, onComplete: animateFootball})

        startAnimation();
        
        
        
        
        // animateFootball()
    }
    
    function startAnimation(){
        var tl = new TimelineMax();
        //Frame one
        tl.addLabel("frameOne")
            .to(ground, 0.8, { y: -510, ease: power1InOut },"frameOne")
            .to("#copy1", 0.8, { y: 0, opacity:1, ease: Bounce.easeOut },"frameOne")
            .to(football,0.8, { x: 0,y:0, ease: Sine.easeOut, onComplete: stopFootball},"frameOne")
            .to(football,1, { x: 290,y:-910, ease: Sine.easeOut, onStart: animateFootball},"frameOne+=2")
            .to(ground, 0.8, { y: 0, ease: power1InOut, onComplete: stopFootball},"frameOne+=2.2")




    }


    function stopFootball() {
        clearInterval(tID);
    }
    function animateFootball() {
        var position = 200,
            interval = 100,
            diff = 200;     
        tID = setInterval ( 
            function() {
                football.style.backgroundPosition =  -position+'px 0px'; 
                
                if ( position < footballSpritWidth) { 
                    position = position + diff;
                }
                else { 
                    position = 200; 
                }
            } , interval );


            // setTimeout(
            //     function() {
            //         clearInterval(tID);
            //     },1000)
    }


}();
